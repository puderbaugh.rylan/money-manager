package yumm.model;

import java.util.Date;

public class DateRange {
    private Date beginDate;
    private Date endDate;

    public DateRange(Date beginDate, Date endDate) {
        this.beginDate = beginDate;
        this.endDate = endDate;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }
}
