package yumm.model;

import java.util.Date;
import java.util.Objects;

/**
 * Immutable Representation of a deposit transaction
 * Mutators are intentionally omitted from this class since all transactions should be immutable (state should not change)
 */
public class DepositTransaction extends AbstractTransaction {
    private Source source;

    /**
     * Creates an instance of a deposit transaction
     * @param userName for the transaction
     * @param amount for the transaction
     * @param date of the transaction
     * @param source of the transaction
     */
    public DepositTransaction(String userName, double amount, Date date, Source source) {
        super(userName, amount, date);
        this.source = source;
    }

    /**
     * Get the source of the transaction
     * @return source
     */
    public Source getSource() {
        return source;
    }

    /**
     * Converts the provided comma delimited string to an instance of this transaction class
     * @param value string representation
     * @return transaction instance
     */
    public static Transaction toTransaction(String value) {
        String[] transactionParts = value.split(",");

        return new DepositTransaction(
                transactionParts[1],
                Double.parseDouble(transactionParts[2]),
                new Date(Long.parseLong(transactionParts[3])),
                Source.valueOf(transactionParts[4])
        );
    }

    /**
     * Provides a string representation of this class
     * @return string representation
     */
    @Override
    public String toValue() {
        return "DEPOSIT" + "," + userName + "," + amount + "," + date.getTime() + "," + source;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;

        if (object == null || getClass() != object.getClass()) return false;
        DepositTransaction that = (DepositTransaction) object;

        return Double.compare(that.amount, amount) == 0 &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(date, that.date) &&
                source == that.source;
    }

    @Override
    public String toString() {
        return "DepositTransaction{" +
                super.toString() +
                ", source=" + source +
                '}';
    }
}