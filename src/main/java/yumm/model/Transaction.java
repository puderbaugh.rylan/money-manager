package yumm.model;

import java.util.Date;

/**
 * Interface represents common capabilities for both Deposit and Withdraw transactions
 */
public interface Transaction {
    String getUserName();

    double getAmount();

    Date getDate();

    String toValue();
}