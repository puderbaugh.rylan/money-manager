package yumm.model;

import java.awt.*;

/**
 * Enum that represents the source for a deposit
 */
public enum Source {
    EARNINGS(Color.GREEN, 85),
    GIFT(Color.YELLOW, 45),
    WON(Color.YELLOW, 45);

    private Color colorStatus;
    private int weight;

    Source(Color colorStatus, int weight) {
        this.colorStatus = colorStatus;
        this.weight = weight;
    }

    public Color getColorStatus() {
        return colorStatus;
    }

    public int getWeight() {
        return weight;
    }
}