package yumm.model;

/**
 * Represents the two types of transactions this application supports
 */
public enum TransactionType {
    DEPOSIT("Deposit"),
    WITHDRAW("Withdraw");

    private String value;

    TransactionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
