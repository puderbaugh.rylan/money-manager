package yumm.model;

import java.util.Date;

/**
 * Abstract immutable Representation of a transaction
 * Mutators are intentionally omitted from this class since all transactions should be immutable (state should not change)
 */
public abstract class AbstractTransaction implements Transaction, Comparable<Transaction> {
    protected String userName;
    protected double amount;
    protected Date date;

    /**
     * Creates an instance of a generic transaction
     * @param userName for the transaction
     * @param amount for the transaction
     * @param date of the transaction
     */
    protected AbstractTransaction(String userName, double amount, Date date) {
        this.userName = userName;
        this.amount = amount;
        this.date = date;
    }

    /**
     * Get the username for the transaction
     * @return user
     */
    @Override
    public String getUserName() {
        return userName;
    }

    /**
     * Get the amount for the transaction
     * @return amount
     */
    @Override
    public double getAmount() {
        return amount;
    }

    /**
     * Get the date of the transaction
     * @return date
     */
    @Override
    public Date getDate() {
        return date;
    }

    @Override
    public int compareTo(Transaction transaction) {
        if (this == transaction) return 0;

        if (transaction == null) {
            return -1;
        }

        if(transaction.getAmount() < this.amount) {
            return -1;
        } else if (transaction.getAmount() == this.amount) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return "userName='" + userName + '\'' +
                ", amount=" + amount +
                ", date=" + date;
    }
}
