package yumm.model;

/**
 * End users target goal (Future Use)
 */
public class Goal {
    private double balance;
    private int percentage;

    public Goal(double balance, int percentage) {
        this.balance = balance;
        this.percentage = percentage;
    }

    public double getBalance() {
        return balance;
    }

    public int getPercentage() {
        return percentage;
    }
}