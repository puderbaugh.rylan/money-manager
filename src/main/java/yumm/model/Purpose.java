package yumm.model;

import java.awt.*;

/**
 * Enum that represents the purpose for a withdrawal
 */
public enum Purpose {
    ESSENTIAL(Color.GREEN, 85),
    NON_ESSENTIAL(Color.YELLOW, 45),
    FRIVOLOUS(Color.RED, 15);

    private Color colorStatus;
    private int weight;

    Purpose(Color colorStatus, int weight) {
        this.colorStatus = colorStatus;
        this.weight = weight;
    }

    public Color getColorStatus() {
        return colorStatus;
    }

    public int getWeight() {
        return weight;
    }
}