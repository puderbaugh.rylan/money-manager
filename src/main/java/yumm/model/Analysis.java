package yumm.model;

import java.util.Objects;

/**
 * Analysis of a users transactions(Deposit, Withdraw) within an interval
 */
public class Analysis {
    private DateRange dateRange;
    private double positivePercent = 0;
    private double neutralPercent = 0;
    private double negativePercent= 0;
    private double overallPercent = 0;

    /**
     * Creates an analysis instance
     * @param dateRange dates for analysis
     * @param positivePercent percentage of transactions that are considered positive
     * @param neutralPercent percentage of transactions that are considered neutral
     * @param negativePercent percentage of transactions that are considered negative
     * @param overallPercent overall percentage of the users transactions
     */
    public Analysis(DateRange dateRange, double positivePercent, double neutralPercent, double negativePercent, double overallPercent) {
        this.dateRange = dateRange;
        this.positivePercent = positivePercent;
        this.neutralPercent = neutralPercent;
        this.negativePercent = negativePercent;
        this.overallPercent = overallPercent;
    }

    /**
     * Gets dateRange for analysis
     * @return the date range
     */
    public DateRange getDateRange() {
        return dateRange;
    }

    /**
     * Gets percentages of transactions that are considered positive
     * @return the percentage
     */
    public double getPositivePercent() {
        return positivePercent;
    }

    /**
     * Gets percentages of transactions that are considered neutral
     * @return the percentage
     */
    public double getNeutralPercent() {
        return neutralPercent;
    }

    /**
     * Gets percentages of transactions that are considered negative
     * @return the percentage
     */
    public double getNegativePercent() {
        return negativePercent;
    }

    /**
     * Computes overall percentage of the users transactions
     * @return the percentage
     */
    public double getOverallPercent() {
        return overallPercent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Analysis analysis = (Analysis) o;
        return Double.compare(analysis.positivePercent, positivePercent) == 0 && Double.compare(analysis.neutralPercent, neutralPercent) == 0 && Double.compare(analysis.negativePercent, negativePercent) == 0 && Double.compare(analysis.overallPercent, overallPercent) == 0 && Objects.equals(dateRange, analysis.dateRange);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateRange, positivePercent, neutralPercent, negativePercent, overallPercent);
    }

    @Override
    public String toString() {
        return "Analysis{" +
                "dateRange=" + dateRange +
                ", positivePercent=" + positivePercent +
                ", neutralPercent=" + neutralPercent +
                ", negativePercent=" + negativePercent +
                ", overallPercent=" + overallPercent +
                '}';
    }
}