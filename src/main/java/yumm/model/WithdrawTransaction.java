package yumm.model;

import java.util.Date;
import java.util.Objects;

/**
 * Immutable Representation of a withdraw transaction
 * Mutators are intentionally omitted from this class since all transactions should be immutable (state should not change)
 */
public class WithdrawTransaction extends AbstractTransaction {
    private Purpose purpose;

    /**
     * Creates an instance of a withdraw transaction
     * @param userName for the transaction
     * @param amount for the transaction
     * @param date of the transaction
     * @param purpose of the transaction
     */
    public WithdrawTransaction(String userName, double amount, Date date, Purpose purpose) {
        super(userName, amount, date);
        this.purpose = purpose;
    }

    /**
     * Get the purpose of the transaction
     * @return purpose
     */
    public Purpose getPurpose() {
        return purpose;
    }

    /**
     * Converts the provided comma delimited string to an instance of this transaction class
     * @param value string representation
     * @return transaction instance
     */
    public static Transaction toTransaction(String value) {
        String[] transactionParts = value.split(",");

        return new WithdrawTransaction(
                transactionParts[1],
                Double.parseDouble(transactionParts[2]),
                new Date(Long.parseLong(transactionParts[3])),
                Purpose.valueOf(transactionParts[4])
        );
    }

    /**
     * Provides a string representation of this class
     * @return string representation
     */
    @Override
    public String toValue() {
        return  "WITHDRAW" + "," + userName + "," + amount + "," + date.getTime() + "," + purpose;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;

        if (object == null || getClass() != object.getClass()) return false;
        WithdrawTransaction that = (WithdrawTransaction) object;

        return Double.compare(that.amount, amount) == 0 &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(date, that.date) &&
                purpose == that.purpose;
    }

    @Override
    public String toString() {
        return "WithdrawTransaction{" +
                super.toString() +
                ", purpose=" + purpose +
                '}';
    }
}