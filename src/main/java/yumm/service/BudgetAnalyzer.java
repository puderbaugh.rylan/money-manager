package yumm.service;

import yumm.model.Analysis;
import yumm.model.Transaction;
import java.util.List;

public interface BudgetAnalyzer {
    /**
     * Performs and return analysis on designated transactions
     * @param transactions to perform for which analysis
     * @return analysis computed from given transactions
     */
    Analysis analyze(List<Transaction> transactions);
}
