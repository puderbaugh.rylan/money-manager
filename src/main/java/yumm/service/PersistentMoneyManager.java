package yumm.service;

import yumm.model.DepositTransaction;
import yumm.model.Transaction;
import yumm.model.WithdrawTransaction;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PersistentMoneyManager extends AbstractMoneyManager {
    private static final String TRANSACTION_FILE_NAME_SUFFIX = "transactions.dat";

    /**
     * Rewrite file with existing and designated new transaction. Each transaction is represented on a separate line with
     * its attributes are comma delimited
     * @param transaction new transaction to append to the file
     */
    public boolean execTransaction(Transaction transaction) throws IOException {
        Path filePath = getFilePath(transaction.getUserName());

        if (super.isValid(transaction)) {
            if (filePath.toFile().exists()) {
                Files.writeString(filePath, System.lineSeparator() + transaction.toValue(),
                        StandardCharsets.UTF_8, StandardOpenOption.APPEND);
            } else {
                Files.writeString(filePath, transaction.toValue(),
                        StandardCharsets.UTF_8, StandardOpenOption.CREATE);
            }

            return true;
        }

        return false;
    }

    /**
     * Retrieves all transactions for the designated user
     * @param userName to get the transactions for
     * @return Corresponding list of transactions
     * @throws FileNotFoundException
     */
    @Override
    public List<Transaction> getTransactions(String userName) throws FileNotFoundException {
        List<Transaction> transactions = new ArrayList<>();

        Path path = (getFilePath(userName));

        if (path.toFile().exists()) {
            InputStream inputStream = new FileInputStream(path.toString());

            // Try block to check for exceptions
            try (Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8.name())) {

                String line;
                while (scanner.hasNextLine()) {
                    line = scanner.nextLine();

                    String transactionValue = line.substring(0, line.indexOf(','));

                    if (transactionValue.equals("DEPOSIT")) {
                        transactions.add(DepositTransaction.toTransaction(line));
                    } else {
                        transactions.add(WithdrawTransaction.toTransaction(line));
                    }
                }
            }
        }

        return transactions;
    }

    private Path getFilePath(String userName) {
        Path path = Paths.get("", userName + "-" + TRANSACTION_FILE_NAME_SUFFIX);

        return path;
    }
}