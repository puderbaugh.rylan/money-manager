package yumm.service;

import yumm.model.*;

import java.util.Date;
import java.util.List;

/*
* Performs analysis on the provided transactions to determine how well a user has managed their finances.
*/
public class BudgetAnalyzerImpl implements BudgetAnalyzer {

    /**
     * Performs and return analysis on designated transactions
     * @param transactions to perform for which analysis
     * @return analysis computed from given transactions
     */
    @Override
    public Analysis analyze(List<Transaction> transactions) {
        AnalysisContext analysisCtx = new AnalysisContext();

        if(!transactions.isEmpty()) {
            for (Transaction transaction : transactions) {
                if (transaction instanceof DepositTransaction) {
                    DepositTransaction depositTransaction = (DepositTransaction) transaction;
                    switch (depositTransaction.getSource()) {
                        case EARNINGS -> analysisCtx.positiveAmount =
                                analysisCtx.positiveAmount + depositTransaction.getAmount();
                        case GIFT, WON -> analysisCtx.neutralAmount =
                                analysisCtx.neutralAmount + depositTransaction.getAmount();
                    }
                } else {
                    WithdrawTransaction withdrawTransaction = (WithdrawTransaction) transaction;
                    switch (withdrawTransaction.getPurpose()) {
                        case ESSENTIAL -> analysisCtx.positiveAmount =
                                analysisCtx.positiveAmount - withdrawTransaction.getAmount();
                        case NON_ESSENTIAL -> analysisCtx.neutralAmount =
                                analysisCtx.neutralAmount - withdrawTransaction.getAmount();
                        case FRIVOLOUS -> analysisCtx.negativeAmount =
                                analysisCtx.neutralAmount - withdrawTransaction.getAmount();
                    }
                }

                updateDateRange(analysisCtx, transaction);
            }
        } else {
            // Since there are no transactions, status defaults to neutral(yellow)
            analysisCtx.neutralAmount = 100;
        }

        return performAnalysis(analysisCtx);
    }

    // Can't assume transaction list is in order. Find the earliest beginDate and the latest endDate.
    private void updateDateRange(AnalysisContext analysisCtx, Transaction transaction) {
        if (analysisCtx.beginDate == null) {
            analysisCtx.beginDate = transaction.getDate();
            analysisCtx.endDate = transaction.getDate();
        } else {
            analysisCtx.beginDate = analysisCtx.beginDate.after(transaction.getDate()) ? transaction.getDate() : analysisCtx.beginDate;
            analysisCtx.endDate = analysisCtx.endDate.before(transaction.getDate()) ? transaction.getDate() : analysisCtx.beginDate;
        }
    }

    // TODO Make Smarter
    // Using the given analysisContext, compute the analysis
    private Analysis performAnalysis (AnalysisContext analysisCtx) {
        double totalAmount = analysisCtx.positiveAmount + analysisCtx.neutralAmount + analysisCtx.negativeAmount;
        System.out.println("*** totalAmount: " + totalAmount);
        System.out.println("*** positiveAmount: " + analysisCtx.positiveAmount);
        System.out.println("*** neutralAmount: " + analysisCtx.neutralAmount);
        System.out.println("*** negativeAmount: " + analysisCtx.negativeAmount);

        double positivePercent = analysisCtx.positiveAmount > 0 ?  analysisCtx.positiveAmount /totalAmount * 100 : 0;
        System.out.println("**** positivePercent: " + positivePercent);
        double neutralPercent = analysisCtx.neutralAmount > 0 ? analysisCtx.neutralAmount /totalAmount * 100: 0;
        System.out.println("**** neutralPercent: " + neutralPercent);
        double negativePercent = analysisCtx.negativeAmount > 0 ? analysisCtx.negativeAmount / totalAmount * 100: 0;
        System.out.println("*** negativePercent: " + negativePercent);

        return new Analysis(
                new DateRange(analysisCtx.beginDate, analysisCtx.endDate),
                positivePercent, neutralPercent, negativePercent, 0);
    }

    // Internal helper class to provide temporary state during computation
    private class AnalysisContext {
        private double positiveAmount;
        private double neutralAmount;
        private double negativeAmount;
        private Date beginDate;
        private Date endDate;

        @Override
        public String toString() {
            return "AnalysisContext{" +
                    "positiveAmount=" + positiveAmount +
                    ", neutralAmount=" + neutralAmount +
                    ", negativeAmount=" + negativeAmount +
                    ", beginDate=" + beginDate +
                    ", endDate=" + endDate +
                    '}';
        }
    }
}