package yumm.service;

import yumm.model.DateRange;
import yumm.model.DepositTransaction;
import yumm.model.Transaction;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractMoneyManager implements MoneyManager {

    /**
     * Retreives all transactions for the designated user and within a datRange
     * @param userName to get the transactions for
     * @param dateRange to get the transactions within
     * @return Corresponding list of transactions
     * @throws FileNotFoundException
     */
    @Override
    public List<Transaction> getTransactions(String userName, DateRange dateRange) throws FileNotFoundException {
        List<Transaction> allTransactions = getTransactions(userName);
        List<Transaction> transactions = new ArrayList<>();

        for(Transaction transaction : allTransactions) {
            if (transaction.getDate().after(dateRange.getBeginDate()) &&
                    transaction.getDate().before(dateRange.getEndDate())) {
                transactions.add(transaction);
            }
        }

        return transactions;
    }

    /**
     * Returns the current balance for the designated user
     * @param userName to get the current balance for
     * @return balance for the given userName
     * @throws FileNotFoundException
     */
    @Override
    public Double getCurrentBalance(String userName) throws FileNotFoundException {
        List<Transaction> transactions = getTransactions(userName);
        double currentBalance = 0;

        for(int index = 0; index<transactions.size(); index++) {
            if( transactions.get(index) instanceof DepositTransaction) {
                currentBalance += transactions.get(index).getAmount();
            } else {
                currentBalance -= transactions.get(index).getAmount();
            }
        }

        return currentBalance;
    }

    protected boolean isValid(Transaction transaction) throws FileNotFoundException {
        return transaction instanceof DepositTransaction || transaction.getAmount() <= getCurrentBalance(transaction.getUserName());
    }
}