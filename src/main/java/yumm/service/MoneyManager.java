package yumm.service;

import yumm.model.DateRange;
import yumm.model.Transaction;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface MoneyManager {
    /** Rewrite file with existing and designated new transaction. Each transaction is represented on a separate line with
     * its attributes are comma delimited
     *
     * @param transaction new transaction to append to the file
     */
    boolean execTransaction(Transaction transaction) throws IOException;
    /**
     * Returns the current balance for the designated user
     * @param userName to get the current balance for
     * @return balance for the given userName
     * @throws FileNotFoundException
     */
    Double getCurrentBalance(String userName) throws FileNotFoundException;
    /**
     * Retrieves all transactions for the designated user
     * @param userName to get the transactions for
     * @return Corresponding list of transactions
     * @throws FileNotFoundException
     */
    List<Transaction> getTransactions(String userName) throws FileNotFoundException;
    /**
     * Retreives all transactions for the designated user and within a datRange
     * @param userName to get the transactions for
     * @param dateRange to get the transactions within
     * @return Corresponding list of transactions
     * @throws FileNotFoundException
     */
    List<Transaction> getTransactions(String userName, DateRange dateRange) throws FileNotFoundException;
}