package yumm.service;

import yumm.model.Transaction;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class is used for testing purposes without a filesystem
 */
public class VolatileMoneyManager extends AbstractMoneyManager {
    private Map<String, List<Transaction>> userTransactions = new HashMap<>();


    /**
     * Rewrite list with existing and designated new transaction. Each transaction is represented on a separate element
     * @param transaction new transaction to append to the file
     */
    @Override
    public boolean execTransaction(Transaction transaction) throws IOException {

        if (super.isValid(transaction)) {
            List<Transaction> transactions = getTransactions(transaction.getUserName());
            transactions.add(transaction);

            return true;
        }

        return false;
    }

    /**
     * Retrieves all transactions for the designated user
     * @param userName to get the transactions for
     * @return Corresponding list of transactions
     * @throws FileNotFoundException
     */
    @Override
    public List<Transaction> getTransactions(String userName) throws FileNotFoundException {
        return userTransactions.get(userName);
    }
}