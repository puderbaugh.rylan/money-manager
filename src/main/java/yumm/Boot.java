package yumm;

import yumm.service.BudgetAnalyzer;
import yumm.service.BudgetAnalyzerImpl;
import yumm.service.MoneyManager;
import yumm.service.PersistentMoneyManager;
import yumm.ui.Application;

import java.io.FileNotFoundException;

// TODO Update UML Diagram

/**
 * Bootstraps the application
 */
public class Boot {
    public static void main(String[] args) {
        MoneyManager moneyManager = new PersistentMoneyManager();
        BudgetAnalyzer budgetAnalyzer = new BudgetAnalyzerImpl();

        Application application = new Application(moneyManager, budgetAnalyzer);
        try {
            application.display();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}