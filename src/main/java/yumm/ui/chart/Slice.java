package yumm.ui.chart;

import java.awt.*;

/**
 * A portion of the pie diagram
 */
public class Slice {
    double value;
    Color color;

    public Slice(double value, Color color) {
        this.value = value;
        this.color = color;
    }
}
