package yumm.ui.chart;

import javax.swing.*;
import java.awt.*;

/**
 * Represents pie chart for the transactions
 */
public class Pie extends JComponent {
    private Slice[] slices = {
            new Slice(100, Color.green), new Slice(100, Color.yellow), new Slice(100, Color.red)
    };

    public void setSlices(Slice[] slices) {
        this.slices = slices;
    }

    public void paint(Graphics graphics) {
        super.paint(graphics);
        drawPie((Graphics2D) graphics, getBounds(), slices);
    }

    void drawPie(Graphics2D graphics, Rectangle area, Slice[] slices) {
        double total = 0.0D;

        for (Slice slice : slices) {
            total += slice.value;
        }

        double curValue = 0.0D;
        int startAngle;
        for (Slice slice : slices) {
            startAngle = (int) (curValue * 360 / total);
            int arcAngle = (int) (slice.value * 360 / total);
            graphics.setColor(slice.color);
            graphics.fillArc(area.x, area.y, area.width, area.height, startAngle, arcAngle);
            curValue += slice.value;
        }
    }
}
