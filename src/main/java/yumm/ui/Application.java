package yumm.ui;

import yumm.model.*;
import yumm.service.BudgetAnalyzer;
import yumm.service.MoneyManager;
import yumm.ui.chart.Pie;
import yumm.ui.chart.Slice;

import javax.swing.*;
import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * User interface for the application
 */
public class Application extends JFrame {
    private MoneyManager moneyManager;
    private BudgetAnalyzer budgetAnalyzer;
    private ColorBar statusColorBar = new ColorBar();
    private String userName;

    JLabel lblCurrentBalanceValue = new JLabel("0");
    JTable tblTransactions = new JTable();
    Pie pieChart = new Pie();
    Help help = new Help();

    public Application(MoneyManager moneyManager, BudgetAnalyzer budgetAnalyzer) {
        super("YUMM");
        this.moneyManager = moneyManager;
        this.budgetAnalyzer = budgetAnalyzer;
    }

    /**
     * Creates menu bar for application options and information
     * @return the constructed menu bar
     */
    public JMenuBar createMenuBar() {
        JMenuBar menuBar;
        JMenu menu;
        JMenuItem menuItem;

        menuBar = new JMenuBar();

        menu = new JMenu("Account");
        menuBar.add(menu);

        menuItem = new JMenuItem("Logout");

        menuItem.addActionListener(e -> {
            System.exit(0);
        });

        menu.add(menuItem);

        menu = new JMenu("Help");
        menuBar.add(menu);

        menuItem = new JMenuItem("Getting Started");

        menuItem.addActionListener(e -> {
            help.gettingStarted();
        });

        menu.add(menuItem);

        menuItem = new JMenuItem("About");

        menuItem.addActionListener(e -> {
            help.about();
        });

        menu.add(menuItem);

        return menuBar;
    }

    /**
     * Creates all panels for the application and adds them to the frame
     * @return the constructed tabbed pane
     */
    private JTabbedPane createTabPanel() {
        JTabbedPane tabbedPane = new JTabbedPane();

        // Enable scrolling tabs.
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);

        Component depositPanel = createExecTransactionPanel(TransactionType.DEPOSIT);
        tabbedPane.addTab(TransactionType.DEPOSIT.getValue(), null, depositPanel, "Deposit Income");

        Component withdrawPanel = createExecTransactionPanel(TransactionType.WITHDRAW);
        tabbedPane.addTab(TransactionType.WITHDRAW.getValue(), null, withdrawPanel, "Withdraw Money");

        Component transactionsPanel = createListTransactionsPanel();
        tabbedPane.addTab("Transactions", null, transactionsPanel, "Display Transactions");

        Component analysisPanel = createAnalysisPanel();
        analysisPanel.setPreferredSize(new Dimension(615, 310));
        tabbedPane.addTab("Analysis", null, analysisPanel, "Displays progress within the date range");

        //Add the tabbed pane to this panel.
        add(tabbedPane);

        // Looks for specific tab click events
        tabbedPane.addChangeListener(e -> {
            if (e.getSource() instanceof JTabbedPane) {
                JTabbedPane jTabPane = (JTabbedPane) e.getSource();

                // Executes when transaction tab is clicked
                if (jTabPane.getSelectedIndex() == 2) {
                    try {
                        System.out.println("Repopulating table");
                        TransactionTableModel transactionTableModel = new TransactionTableModel(moneyManager.getTransactions(userName));
                        tblTransactions.setModel(transactionTableModel);
                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                    }

                    // Executes when analysis tab is clicked
                } else if (jTabPane.getSelectedIndex() == 3) {
                    try {
                        List<Transaction> transactionList = moneyManager.getTransactions(userName);
                        Analysis analysis = budgetAnalyzer.analyze(transactionList);

                        System.out.println(analysis.getPositivePercent());
                        System.out.println(analysis.getNeutralPercent());
                        System.out.println(analysis.getNegativePercent());

                        pieChart.setSlices(new Slice[] {
                                new Slice(analysis.getPositivePercent(), Color.green),
                                new Slice(analysis.getNeutralPercent(), Color.yellow),
                                new Slice(analysis.getNegativePercent(), Color.red)});
                    } catch (FileNotFoundException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });

        return tabbedPane;
    }

    private JComponent createListTransactionsPanel() {
        JPanel panel = new JPanel();

        panel.add(new JScrollPane(tblTransactions));
        panel.invalidate();

        return panel;
    }

    private JComponent createExecTransactionPanel(TransactionType transactionType) {
        JPanel panel = new JPanel(false);
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        JLabel lblAmount = new JLabel("Amount $");
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets.top = 0;
        constraints.insets.bottom = 100;
        constraints.insets.left = 25;
        panel.add(lblAmount, constraints);

        JTextField txtAmount = new JTextField();
        txtAmount.setText("0");
        txtAmount.setColumns(10);
        constraints.anchor = GridBagConstraints.WEST;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 1.0;
        panel.add(txtAmount, constraints);

        JLabel lblReason = new JLabel("Reason");
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets.top = 0;
        constraints.insets.bottom = 25;
        constraints.insets.left = 25;
        panel.add(lblReason, constraints);

        JComboBox cmbReason = new JComboBox(
                transactionType == TransactionType.DEPOSIT ?
                Source.values() :
                Purpose.values()
        );
        txtAmount.setText("0");
        txtAmount.setColumns(10);
        constraints.anchor = GridBagConstraints.WEST;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.weightx = 1.0;
        panel.add(cmbReason, constraints);

        JButton btnTransaction = new JButton(transactionType.getValue());
        constraints.anchor = GridBagConstraints.EAST;
        constraints.gridx = 2;
        constraints.gridy = 1;
        constraints.insets.left = 2;
        constraints.insets.top = 10;
        constraints.insets.bottom = 10;
        constraints.insets.right = 50;
        constraints.weightx = 0;
        panel.add(btnTransaction, constraints);


        // Executes when the transaction button is clicked
        btnTransaction.addActionListener(e -> {
            try {
                Transaction transaction;

                // Executes when on the deposit tab and the transaction button is clicked
                if (transactionType == TransactionType.DEPOSIT) {
                    transaction = new DepositTransaction(
                            userName,
                            Integer.parseInt(txtAmount.getText()),
                            new Date(),
                            Source.valueOf(cmbReason.getSelectedItem().toString())
                    );

                    // Executes when on the withdraw tab and the transaction button is clicked
                } else {
                    transaction = new WithdrawTransaction(
                            userName,
                            Integer.parseInt(txtAmount.getText()),
                            new Date(),
                            Purpose.valueOf(cmbReason.getSelectedItem().toString())
                    );
                }

                moneyManager.execTransaction(transaction);

                lblCurrentBalanceValue.setText(moneyManager.getCurrentBalance(userName).toString());
                statusColorBar.setColor(getStatusColor());
                statusColorBar.setVisible(true);
                statusColorBar.setPreferredSize(new Dimension(400, 30));
                statusColorBar.invalidate();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        return panel;
    }

    protected JComponent createAnalysisPanel() {
        JPanel panel = new JPanel(false);
        panel.setLayout(new BorderLayout());
        pieChart.setVisible(true);
        panel.add(pieChart, BorderLayout.CENTER);
        panel.add(legendPanel(), BorderLayout.SOUTH);

        return panel;
    }

    protected JComponent createSummaryPanel() throws FileNotFoundException {
        lblCurrentBalanceValue.setText(moneyManager.getCurrentBalance(userName).toString());

        JPanel panel = new JPanel(false);
        panel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        JLabel lblName = new JLabel("Name:");
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets.left = 2;
        constraints.insets.top = 10;
        constraints.insets.bottom = 10;
        panel.add(lblName, constraints);

        JLabel lblNameValue = new JLabel(userName);
        constraints.anchor = GridBagConstraints.WEST;
        constraints.insets.right = 25;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.insets.bottom = 10;
        constraints.weightx = 1.0;
        panel.add(lblNameValue, constraints);

        JLabel lblStatus = new JLabel("Status:");
        constraints.anchor = GridBagConstraints.EAST;
        constraints.gridx = 2;
        constraints.gridy = 0;
        constraints.insets.left = 2;
        constraints.insets.top = 10;
        constraints.insets.bottom = 10;
        constraints.weightx = 0;
        panel.add(lblStatus, constraints);

        constraints.anchor = GridBagConstraints.EAST;
        constraints.gridx = 3;
        constraints.gridy = 0;
//        constraints.insets.top = 10;
//        constraints.insets.bottom = 10;
        statusColorBar.setColor(getStatusColor());
        statusColorBar.setVisible(true);
        statusColorBar.setPreferredSize(new Dimension(400, 30));
        panel.add(statusColorBar, constraints);

        JLabel lblCurrentBalance = new JLabel("Balance: $");
        constraints.anchor = GridBagConstraints.WEST;
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.weightx = 0;
        panel.add(lblCurrentBalance, constraints);

        constraints.anchor = GridBagConstraints.WEST;
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        panel.add(lblCurrentBalanceValue, constraints);

        return panel;
    }

    public JComponent legendPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(3, 2, 1, 1));

        panel.add(new JLabel("Positive"));
        panel.add(new ColorBar(Color.GREEN));
        
        panel.add(new JLabel("Neutral"));
        panel.add(new ColorBar(Color.YELLOW));

        panel.add(new JLabel("Negative"));
        panel.add(new ColorBar(Color.RED));

        return panel;
    }

    private Color getStatusColor() {
        Color color = Color.YELLOW;

        try {
            List<Transaction> transactionList = moneyManager.getTransactions(userName);
            Analysis analysis = budgetAnalyzer.analyze(transactionList);

            System.out.println(analysis.getPositivePercent());
            System.out.println(analysis.getNeutralPercent());
            System.out.println(analysis.getNegativePercent());

            if(analysis.getPositivePercent() > analysis.getNeutralPercent() && analysis.getPositivePercent() > analysis.getNegativePercent()) {
                color = Color.GREEN;
            } else if (analysis.getNegativePercent() > analysis.getPositivePercent() && analysis.getNegativePercent() > analysis.getNeutralPercent()) {
                color = Color.RED;
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        return color;
    }

    /**
     * Displays tab panel within the frame
     * @throws FileNotFoundException can't find transaction file for the user
     */
    public void display() throws FileNotFoundException {
        JTabbedPane panTabs = createTabPanel();

        userName = JOptionPane.showInputDialog(this, "Enter your name: ");

        this.setLocationRelativeTo(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setJMenuBar(createMenuBar());
        this.add(createSummaryPanel(), BorderLayout.NORTH);
        this.add(panTabs, BorderLayout.CENTER);
        this.pack();
        this.setVisible(true);
    }
}