package yumm.ui;

import javax.swing.*;
import java.awt.*;

public class ColorBar extends JComponent {
    private static final int DEFAULT_WIDTH = 80;
    private static final int DEFAULT_HEIGHT = 10;

    private int width;
    private int height;
    private Color color;
    public ColorBar () {
        this(DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public ColorBar (Color color) {
        this(color, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    public ColorBar (Color color, int width, int height) {
        this.color = color;
        this.width = width;
        this.height = height;
    }

    public ColorBar (int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public void paint(Graphics graphics) {
        super.paint(graphics);

        Graphics2D graphics2D = (Graphics2D) graphics;
        graphics2D.setColor(color);

        graphics2D.drawRect(0, 10, width, height);
        graphics2D.setColor(color);
        graphics2D.fillRect(0, 10, width, height);
    }
}