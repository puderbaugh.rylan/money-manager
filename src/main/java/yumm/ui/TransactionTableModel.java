package yumm.ui;

import yumm.model.DepositTransaction;
import yumm.model.Transaction;
import yumm.model.TransactionType;
import yumm.model.WithdrawTransaction;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * MVC Model to populate the transaction table
 */
public class TransactionTableModel extends AbstractTableModel {
    // Headers for the column of the table
    private String[] columns = new String[] {
            "Transaction Type",
            "Amount",
            "Date",
            "Reason"};
    private List<Transaction> transactionList;

    public TransactionTableModel(List<Transaction> transactionList) {
        this.transactionList = transactionList;
    }

    @Override
    public int getRowCount() {
        return transactionList.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    public String getColumnName(int col) {
        return columns[col];
    }

    /**
     * Gives back the cell at the particular coordinate
     * @param rowIndex within table
     * @param columnIndex within table
     * @return value at the corresponding row and column index
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object cell = null;

        TransactionType transactionType = transactionList.get(rowIndex) instanceof DepositTransaction ?
                TransactionType.DEPOSIT :
                TransactionType.WITHDRAW;

        String reason = transactionList.get(rowIndex) instanceof DepositTransaction ?
                ((DepositTransaction)transactionList.get(rowIndex)).getSource().toString() :
                ((WithdrawTransaction)transactionList.get(rowIndex)).getPurpose().toString();

        switch(columnIndex) {
            case 0 -> cell = transactionType.getValue();
            case 1 -> cell = transactionList.get(rowIndex).getAmount();
            case 2 -> cell = transactionList.get(rowIndex).getDate();
            case 3 -> cell = reason;
        }

        return cell;
    }
}