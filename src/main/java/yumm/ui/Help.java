package yumm.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Represents information about the application and it's features
 */
public class Help {
    private Dimension dimension;

    public Help() {
        this(new Dimension(615, 150));
    }

    public Help(Dimension dimension) {
        this.dimension = dimension;
    }

    public void about() {
        popup("About",
                "This application was designed to help individuals manage money.\n" +
                        "The Deposit tab is to deposit money.\n" +
                        "the Withdraw tab is to withdraw money you have deposited.\n" +
                        "The Transactions tab displays deposits and withdraws you have made.\n" +
                        "The Analysis tab displays a graph based on your status of deposits and withdraws.");
    }

    public void gettingStarted() {
        popup("Getting Started", "To start using YUMM, click on the Deposit tab and deposit money.\n" +
                "You can enter the amount of money to deposit, as well as the reason you are depositing.\n" +
                "Specifying a reason is so the program can perform an analysis on the deposit transaction.\n" +
                "You can then withdraw money that you currently have and also give a purpose in the Withdraw tab.\n" +
                "Specifying a purpose is also so the program can perform an analysis on the withdraw transaction.\n" +
                "To get a list of transactions you have made, click on the Transactions tab.\n" +
                "Lastly, to view the analysis the program has made so far, click the Analysis tab.");
    }

    public void popup(String title, String information) {
        JFrame frame = new JFrame(title);

        JPanel panel = new JPanel();

        frame.add(panel);

        JTextArea textInfo = new JTextArea(information);
        textInfo.setEditable(false);
        textInfo.setColumns(50);
        panel.add(textInfo);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setSize(dimension);
    }
}