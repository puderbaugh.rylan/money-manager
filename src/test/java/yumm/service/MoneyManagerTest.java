package yumm.service;

import yumm.model.*;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class MoneyManagerTest {
    MoneyManager moneyManager = new PersistentMoneyManager();

    public void performTransactionTest() throws Exception {
        System.out.println("**** Perform transaction test begin ****");

        Date currentDate = new Date();
        moneyManager.execTransaction(new DepositTransaction("lex", 150, currentDate, Source.GIFT));
        System.out.println("Current Balance: " + moneyManager.getCurrentBalance("lex") + "\n");

        Date currentDataPlusOneDay = toDate(toLocalDate(currentDate).plusDays(1));
        System.out.println("Withdraw Status: " + (moneyManager.execTransaction(
                new WithdrawTransaction(
                        "lex",
                        25,
                        currentDataPlusOneDay,
                        Purpose.FRIVOLOUS)) ? "SUCCESS" : "FAILURE"));
        System.out.println("Current Balance: " + moneyManager.getCurrentBalance("lex") + "\n");

        Date currentDataPlusOneMonth = toDate(toLocalDate(currentDate).plusMonths(1));
        moneyManager.execTransaction(new DepositTransaction("lex", 100, currentDataPlusOneMonth, Source.GIFT));
        System.out.println("Current Balance: " + moneyManager.getCurrentBalance("lex") + "\n");

        Date currentDataMinus5Month = toDate(toLocalDate(currentDate).minusMonths(5));
        System.out.println("Withdraw Status: " + (moneyManager.execTransaction(
                new WithdrawTransaction(
                        "lex",
                        247,
                        currentDataMinus5Month,
                        Purpose.ESSENTIAL)) ? "SUCCESS" : "FAILURE"));
        System.out.println("Current Balance: " + moneyManager.getCurrentBalance("lex") + "\n");

        // calls nested for loop
        for (Transaction transaction: moneyManager.getTransactions("lex")) {
            System.out.println(transaction);
        }

        System.out.println("**** Perform transaction test end ****\n\n");
    }

    public void listTransactionsTest() throws Exception {
        System.out.println("**** List transaction test begin ****");

        for (Transaction transaction: moneyManager.getTransactions("mom")) {
            System.out.println(transaction);
        }

        System.out.println("**** List transaction test begin ****\n\n");
    }

    private Path getBasePath() {
        ClassLoader classLoader = getClass().getClassLoader();
        String path = classLoader.getResource("").getPath();

        return Path.of(path);
    }

    private Path getFilePath(String userName) {
        Path path = Paths.get(getBasePath().toString(), userName + "-" + "somethingRandom.dat");

        return path;
    }

    private LocalDateTime toLocalDate(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }

    private Date toDate(LocalDateTime dateToConvert) {
        return java.util.Date
                .from(dateToConvert.atZone(ZoneId.systemDefault())
                        .toInstant());
    }

    public void exec() {
        try {
            performTransactionTest();
        } catch (Exception e) {
            System.out.println("Withdraw Failed");
            e.printStackTrace();
        }
        try {
            listTransactionsTest();
        } catch (Exception e) {
            System.out.println("List Transactions Failed");
            e.printStackTrace();
        }

        getFilePath("mom");

        getBasePath();
    }

    public static void main(String[] args) throws IOException {
        MoneyManagerTest moneyManagerTest = new MoneyManagerTest();
        moneyManagerTest.exec();
    }
}