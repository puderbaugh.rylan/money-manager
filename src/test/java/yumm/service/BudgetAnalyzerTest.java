package yumm.service;

import yumm.model.*;

import java.util.Arrays;
import java.util.Date;


public class BudgetAnalyzerTest {

    public void testAnalyze() throws Exception {
        Date firstDate = new Date();
        Date lastDate = new Date();

        Analysis expectedAnalysis = new Analysis(new DateRange(firstDate, lastDate), 85, 0,0,0);

        Transaction transactions[] = new Transaction[]{
                new WithdrawTransaction("ejfudd", 10000000, firstDate, Purpose.ESSENTIAL),
                new WithdrawTransaction("bbunny", 0.1, lastDate, Purpose.FRIVOLOUS)
        };

        BudgetAnalyzer budgetAnalyzer = new BudgetAnalyzerImpl();
        Analysis actualAnalysis = budgetAnalyzer.analyze(Arrays.asList(transactions));


        System.out.println(actualAnalysis);
    }

    public void exec() {
        try {
            testAnalyze();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new BudgetAnalyzerTest().exec();
    }
}
